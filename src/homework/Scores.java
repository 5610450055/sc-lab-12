package homework;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Scores {
	
	String fileR1;
	String fileR2;
	String fileW;
	FileReader fileReader;
	FileWriter fileWriter;
	
	public Scores(String fileR1 ,String fileR2 ,String fileW) {
		this.fileR1 = fileR1;
		this.fileR2 = fileR2;
		this.fileW = fileW;
	}

	public void average() {
		double average = 0;
		try{
			fileReader = new FileReader(this.fileR1);
			fileWriter = new FileWriter(fileW, true);
			BufferedReader buffer = new BufferedReader(fileReader);
			PrintWriter output = new PrintWriter(new BufferedWriter(fileWriter));
			
			output.println("\tHomework Scores");
			output.println("Name   Average");
			output.println("=====  =========");
			String line;
			
			for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				String[] s = line.split(", ");
				for(int i = 1; i < s.length; i++){
					average += Double.parseDouble(s[i]);
				}
				average = average/(s.length - 1);
				output.println(s[0]+"\t"+average);
				average = 0;
			}
			
			fileReader = new FileReader(this.fileR2);
			BufferedReader buffer2 = new BufferedReader(fileReader);
			
			output.println("\tExam Scores");
			output.println("Name Average");
			output.println("==== =======");
			
			for(line = buffer2.readLine(); line != null; line = buffer2.readLine()){
				String[] s = line.split(", ");
				for(int i = 1; i < s.length; i++){
					average += Double.parseDouble(s[i]);
				}
				average = average/(s.length - 1);
				output.println(s[0]+"\t"+average);
				output.flush();
				average = 0;
			}
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+ fileR1);
		}	 	
		catch (IOException e){
			System.err.println("Error reading from file" + fileR1);
		}
		finally{
			try {
				if (fileReader != null)
					fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
	}
}
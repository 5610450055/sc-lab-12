package wordcounter;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class WordCounter {
	
	private String message;
	private HashMap<String, Integer> wordCount ;
	FileReader fileReader ;
	
	public WordCounter(String message){
		this.message = message;
		wordCount = new HashMap<String, Integer>();
		
	}
	
	public void count(){
		try{
			fileReader = new FileReader(message);
			BufferedReader buffer = new BufferedReader(fileReader);
			for (String line = buffer.readLine(); line != null; line = buffer.readLine()) {
				String[] words = line.split(" ");
				for (int i = 0; i<words.length; i++) {
		            if (wordCount.containsKey(words[i]) == false) {
		            	wordCount.put(words[i], 1);
		            } 
		            else {
		            	wordCount.replace(words[i], wordCount.get(words[i]), wordCount.get(words[i])+1);
		            }
		        }
			}
		}
		catch(FileNotFoundException e){
			System.err.println("File Not Found " + fileReader);
		}
		catch(IOException e){
			System.err.println("Input error");
		}
	}
	
	public String hasWord() {
		String s=null;
		for (String key : wordCount.keySet()) {
			s = s+key + " " + wordCount.get(key) + "\n";
		}
		return s;
	}
}

package phoneBook;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class PhoneBook {
	
	private String file;
	private FileReader reader;
	
	public PhoneBook(String file) {
		this.file = file;
	}
	
	public void readList() {
		try {
			System.out.println("\tPhone List");
			reader = new FileReader(this.file);
			BufferedReader buffer = new BufferedReader(reader);
			String line;
			int number = 1;
			
			for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				System.out.println(number+".) " + line);
				number++;
			}
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file : "+file);
		}
		catch (IOException e) {
			System.err.println("Error reading from file");
		}
		
		finally {
			try {
				if (reader != null)
					reader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
	}

}

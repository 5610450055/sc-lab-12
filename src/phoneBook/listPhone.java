package phoneBook;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class listPhone {
	
	String file;
	FileWriter fileWriter;
	
	public listPhone(String file){
		this.file = file;
	}
	
	public void addPhone() {
		try {
			InputStreamReader inReader = new InputStreamReader(System.in);
			BufferedReader buffer = new BufferedReader(inReader);
		
			fileWriter = new FileWriter(this.file,true);
			PrintWriter out = new PrintWriter(new BufferedWriter(fileWriter));
			String line = buffer.readLine();
			int count = 1;
			while (!line.equals("2")) {
				if (count==1) {
					out.println();
					count=0;
				}
				out.println(line);
				line = buffer.readLine();
			}
			out.flush();
		}
		catch (NumberFormatException e) {
			System.err.println("Plese enter name phone!!");
		}
		catch (FileNotFoundException e) {
			System.err.println("Cannot read file : "+file);
		}
		catch (IOException e) {
			System.err.println("Error reading from file");
		}
		
		finally {
			try {
				if (fileWriter!= null)
					fileWriter.close();
			} catch (IOException e) {
			System.err.println("Error closing files");
			}
		}
	}
}
